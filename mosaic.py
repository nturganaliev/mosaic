#! /usr/bin/env python2

from __future__ import print_function

import math
import os
import random
import sys
import time

from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
    FileTransferSpeed, FormatLabel, Percentage, \
    ProgressBar, ReverseBar, RotatingMarker, \
    SimpleProgress, Timer, AdaptiveETA, AbsoluteETA, AdaptiveTransferSpeed

from PIL import Image



class MosaicTemplate:
    def __init__(self, template_img, resize_value):
        self.template_image = Image.open(template_img)
        self.resize_value = resize_value

    def get_template_image(self):
        return self.template_image

    def get_template_image_size(self):
        return self.template_image.size

    def template_resize(self):
        image = self.template_image

        width_percent = (self.resize_value / float(image.size[0]))
        height = int((float(image.size[1]) * float(width_percent)))

        # Resize it.
        image = image.resize((self.resize_value, height), Image.BILINEAR)

        # write image
        image.save(os.path.join('template_ready_to_use', 'template_output.jpg'))


class MosaicSource:
    def __init__(self, image_dir):
        self.image_dir = image_dir

    def source_resize(self):
        for name in os.listdir(self.image_dir):
            path = os.path.join(self.image_dir, name)
            image = Image.open(path)

            # crop image to square (anchor at centre)
            w, h = image.size
            if w != h:
                if w > h:
                    d = float(w - h) / 2
                    box = (d, 0, w - d, h)
                elif h > w:
                    d = float(h - w) / 2
                    box = (0, d, w, h - d)

            box = map(lambda x: int(x), box)
            image = image.crop(box=box)

            # resize
            image = image.resize(size=(50, 50))

            # write image
            image.save(os.path.join('source_ready_to_use', name))


class Mosaic:
    def __init__(self, template_img, source):
        self.template_img = Image.open(template_img)
        self.source = source

    def distance(self, a, b):
        def _difference(a, b):
            if a > b:
                return a - b
            elif b > a:
                return b - a
            else:
                return 0

        assert len(a) == len(b)
        dist = 0
        for i in range(0, len(a)):
            dist += _difference(a[i], b[i])
        return dist

    def average_colour(self, pixels, img_size):
        channels = len(pixels[0, 0])
        colours = [0] * channels
        for x in range(0, img_size[0]):
            for y in range(0, img_size[1]):
                for channel in range(0, channels):
                    colours[channel] += pixels[x, y][channel]

        colours = list(map(lambda x: int(x / (img_size[0] * img_size[1])), colours))
        return colours


    def generate_mosaic(self):
        PIXEL_SIZE = 64

        images = []
        for name in os.listdir(self.source):
            path = os.path.join(self.source, name)

            image = Image.open(path)
            image = image.resize((PIXEL_SIZE, PIXEL_SIZE))
            pixels = image.load()
            img_size = image.size

            images.append({
                "image": image,
                "pixels": pixels,
                "path": path,
                "colour": self.average_colour(pixels, img_size)
            })

        image = self.template_img
        pixels = image.load()
        w, h = image.size

        output_dims = (w * PIXEL_SIZE, h * PIXEL_SIZE)
        output_image = Image.new("RGB", output_dims)

        pos_w = 0
        pos_h = 0
        for x in range(0, w):
            for y in range(0, h):
                for i in images:
                    i["distance"] = self.distance(pixels[x, y], i["colour"])
                best_matches = sorted(images, key=lambda a: a["distance"])
                choice = random.choice(best_matches[0:5])

                output_image.paste(choice["image"], (pos_w, pos_h))

                pos_h += PIXEL_SIZE
                if pos_h == output_dims[1]:
                    pos_h = 0
                    pos_w += PIXEL_SIZE

        output_image.save("output.jpg")


def example(fn):
    def wrapped():
        try:
            sys.stdout.write('Running: %s\n' % fn.__name__)
            fn()
            sys.stdout.write('\n')
        except KeyboardInterrupt:
            sys.stdout.write('\nSkipping example.\n\n')

    return wrapped


@example
def with_example0():
    with ProgressBar(max_value=10) as progress:
        for i in range(10):
            mosaic_template = MosaicTemplate('raw_template/5x5-nurlan.jpg', 45)
            mosaic_template.template_resize()

            mosaic_src = MosaicSource('raw_source')
            mosaic_src.source_resize()

            mosaic = Mosaic('template_ready_to_use/template_output.jpg', 'source_ready_to_use')
            mosaic.generate_mosaic()

            time.sleep(0.001)
            progress.update(i)



if __name__ == '__main__':

    with_example0()

    # mosaic_template = MosaicTemplate('raw_template/5x5-nurlan.jpg', 45)
    # mosaic_template.template_resize()
    #
    # mosaic_src = MosaicSource('raw_source')
    # mosaic_src.source_resize()
    #
    # mosaic = Mosaic('template_ready_to_use/template_output.jpg', 'source_ready_to_use')
    # mosaic.generate_mosaic()
