install the following dependencies first:

1) PIL (Pillow)
2) Progress bar: git clone https://github.com/WoLpH/python-progressbar.git


## STRUCTURE OF THE FOLDERS AND HOW DO THEY WORK

1) the folder raw_template works with the following line of code:

     mosaic_template = MosaicTemplate('raw_template/name_of_the_template_image.jpg', 45)
     mosaic_template.template_resize()

     Image after resize will be saved in folder: template_ready_to_use

2) the folder raw_source works with the following piece of code:

    mosaic_src = MosaicSource('raw_source')
    mosaic_src.source_resize()

    Images in folder raw_source will be resized and saved in saved in folder: source_ready_to_use

3) this line of code:

    mosaic = Mosaic('template_ready_to_use/template_output.jpg', 'source_ready_to_use')
    mosaic.generate_mosaic()

    generates the mosaic
    result will be saved in the root folder under name output.jpg
